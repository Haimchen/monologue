# Monologue

A simple way to talk to yourself.


### Sources

Based on `todo-modern` from [relay-examples](https://github.com/relayjs/relay-examples)

### Running this app

```
yarn
yarn update-schema
yarn build
yarn start
```
