export class Message {}
export class User {}

const VIEWER_ID = 'me'

const viewer = new User()
viewer.id = VIEWER_ID

const usersById = {
  [VIEWER_ID]: viewer,
}

const messages = []
let nextMessageId = 0

addMessage('Hallo')

export function addMessage(text) {
  const message = new Message()
  message.id = nextMessageId++
  message.text = text
  message.createdAt = new Date() / 1000
  messages.push(message)
  return message.id
}

export function getMessage(id) {
  const numericId = Number(id)
  return messages.find(m => m.id === numericId)
}

export function getMessages() {
  return messages
}

export function getUser(id) {
  return usersById[id]
}

export function getViewer() {
  return getUser(VIEWER_ID)
}

export function removeMessage(id) {
  const messageIndex = messages.indexOf(getMessage(id))
  if (messageIndex !== -1) {
    messages.splice(messageIndex, 1)
  }
}

export function editMessage(id, text) {
  const message = getMessage(id)
  message.text = text
}
