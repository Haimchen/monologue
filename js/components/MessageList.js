import Message from './Message'

import React from 'react'

import {
  createFragmentContainer,
  graphql,
} from 'react-relay'

class MessageList extends React.Component {
  componentDidUpdate(prevProps) {
    if (prevProps.viewer.totalCount !== this.props.viewer.totalCount) {
      if (this.messageList) {
        this.messageList.scrollTop = this.messageList.scrollHeight
      }
    }
  }

  renderMessages() {
    return this.props.viewer.messages.edges.map(edge =>
      <Message
        key={edge.node.id}
        message={edge.node}
        viewer={this.props.viewer}
      />
    )
  }

  render() {
    return (
      <section className="main" ref={ (r) => this.messageList = r }>
        <ul className="message-list">
          {this.renderMessages()}
        </ul>
      </section>
    )
  }
}

export default createFragmentContainer(MessageList, {
  viewer: graphql`
    fragment MessageList_viewer on User {
      messages(
        first: 2147483647  # max GraphQLInt
      ) @connection(key: "MessageList_messages") {
        edges {
          node {
            id,
            ...Message_message,
          },
        },
      },
      id,
      totalCount,
      ...Message_viewer,
    }
  `,
})
