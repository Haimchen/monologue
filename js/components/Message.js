import RemoveMessageMutation from '../mutations/RemoveMessageMutation'
import EditMessageMutation from '../mutations/EditMessageMutation'
import MessageTextInput from './MessageTextInput'

import React from 'react'
import {
  createFragmentContainer,
  graphql,
} from 'react-relay'

import Moment from 'moment'

import classnames from 'classnames'

class Message extends React.Component {
  state = {
    isEditing: false,
  }
  _handleDestroyClick = () => {
    this._removeMessage()
  }
  _handleEditClick = () => {
    this._setEditMode(true)
  }
  _handleTextInputCancel = () => {
    this._setEditMode(false)
  }
  _handleTextInputDelete = () => {
    this._setEditMode(false)
    this._removeMessage()
  }
  _handleTextInputSave = (text) => {
    this._setEditMode(false)
    EditMessageMutation.commit(
      this.props.relay.environment,
      text,
      this.props.message,
    )
  }
  _removeMessage() {
    RemoveMessageMutation.commit(
      this.props.relay.environment,
      this.props.message,
      this.props.viewer,
    )
  }
  _setEditMode = (shouldEdit) => {
    this.setState({isEditing: shouldEdit})
  }
  renderTextInput() {
    return (
      <MessageTextInput
        className="input-message"
        commitOnBlur={true}
        initialValue={this.props.message.text}
        onCancel={this._handleTextInputCancel}
        onDelete={this._handleTextInputDelete}
        onSave={this._handleTextInputSave}
      />
    )
  }
  render() {
    return (
      <li
        className={classnames({
          editing: this.state.isEditing,
        })}>
        <div className="message">
          <div className="message-avatar">SK</div>
          <p className="message-bubble">
            {this.state.isEditing && this.renderTextInput()}
            {!this.state.isEditing &&
              <span className="message-text">{this.props.message.text}</span>
            }
            {!this.state.isEditing &&
              <span className="message-buttons">
                <a className="icon-button" onClick={this._handleEditClick}>
                  <i className='fa fa-pencil' />
                </a>
                <a className="icon-button" onClick={this._handleDestroyClick}>
                  <i className='fa fa-times' />
                </a>
              </span>
            }
          </p>
        </div>
        <p className='message-timestamp'>{Moment.unix(this.props.message.createdAt).format('D/M/YYYY HH:mm')}</p>
      </li>
    )
  }
}

export default createFragmentContainer(Message, {
  message: graphql`
    fragment Message_message on Message {
      id,
      text,
      createdAt
    }
  `,
  viewer: graphql`
    fragment Message_viewer on User {
      id,
      totalCount,
    }
  `,
})
