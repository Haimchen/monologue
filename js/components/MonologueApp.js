import React from 'react'
import {
  createFragmentContainer,
  graphql,
} from 'react-relay'

import AddMessageMutation from '../mutations/AddMessageMutation'
import MessageList from './MessageList'
import MessageTextInput from './MessageTextInput'

class MessageApp extends React.Component {
  _handleTextInputSave = (text) => {
    AddMessageMutation.commit(
      this.props.relay.environment,
      text,
      this.props.viewer,
    )
  }

  render() {
    const numMessages = this.props.viewer.totalCount
    const ITEM = numMessages === 1 ? 'item' : 'items'

    return (
      <div className="container">
        <section className="message-app">
          <header className="header">
            <h4>{ numMessages } { ITEM }</h4>
          </header>
          <MessageList viewer={ this.props.viewer }/>
          <MessageTextInput
            autoFocus={true}
            className="input-message"
            onSave={this._handleTextInputSave}
            placeholder="Enter your message"
          />
        </section>
      </div>
    )
  }
}

export default createFragmentContainer(MessageApp, {
  viewer: graphql`
    fragment MonologueApp_viewer on User {
      totalCount,
      id,
      ...MessageList_viewer,
    }
  `,
})
